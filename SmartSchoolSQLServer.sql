
CREATE DATABASE SmartSchool
GO
USE SmartSchool
GO
CREATE TABLE person(
personId INT PRIMARY KEY IDENTITY,
firstName VARCHAR(25),
secondName VARCHAR(25),
firstLastName VARCHAR(25),
secondLastName VARCHAR(25),
gender TINYINT,
birthday DATE,
address VARCHAR(100),
photo IMAGE,
--state BIT
)

GO
CREATE TABLE employees(
employeeId INT PRIMARY KEY IDENTITY,
personId INT FOREIGN KEY REFERENCES dbo.person(personId),
role INT, -- 1 AcademyStaff, 2 Cajero, 3 Seguridad, 4 Afanador, 5 Workforce,6 Sr�o. Academcio, 7 Director, 8 Sub-Director, 9 Profesor, 10 Supervisor
identityCard VARCHAR(16),
phone VARCHAR(9),
email VARCHAR(50),
state BIT
)

GO
CREATE TABLE tutor(
tutorId INT PRIMARY KEY IDENTITY,
personId INT FOREIGN KEY REFERENCES dbo.person(personId),
identityCard VARCHAR(16),
phone VARCHAR(9),
email VARCHAR(50),
job VARCHAR(50),
workPlace VARCHAR(50),
state BIT
)

GO
CREATE TABLE student(
studentId INT PRIMARY KEY IDENTITY,
personId INT FOREIGN KEY REFERENCES dbo.person(personId),
studentCode VARCHAR(30),
state BIT
)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GO
CREATE TABLE studentTutor(
studentTutor INT PRIMARY KEY IDENTITY,
studentId INT FOREIGN KEY REFERENCES dbo.student(studentId),
tutorId INT FOREIGN KEY REFERENCES dbo.tutor(tutorId) 
)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GO
CREATE TABLE users(
userId INT PRIMARY KEY IDENTITY,
personId INT FOREIGN KEY REFERENCES dbo.person(personId),
userU VARCHAR(30),
passwordU VARCHAR(64),
state BIT
)

GO
CREATE TABLE role(
roleId INT PRIMARY KEY IDENTITY,
userId INT FOREIGN KEY REFERENCES dbo.users(userId),
employe_access bit,
users_access bit,
subjects_access bit,
grades_access bit,
sections_access bit,
duty_access bit, 
enrollment_access bit,
cash_access bit,
addGradeNote_access bit,
assignmentSubjects_access bit,
horary_access bit,
bestStudentsReport_access bit,
paymentsReport_access bit,
monthlyPaymentsReport_access bit,
recordReport_access bit
)

------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GO
CREATE TABLE subject(
subjectId INT PRIMARY KEY IDENTITY,
subjectName VARCHAR(25),
description VARCHAR(100),
frecuencyClass INT,
state BIT
)


GO
CREATE TABLE classrooms(
classroomsId INT PRIMARY KEY IDENTITY,
codeClassroom VARCHAR(5),
capacity INT,
state bit
)

GO
CREATE TABLE grades(
gradeId INT PRIMARY KEY IDENTITY,
classroomId INT FOREIGN KEY REFERENCES dbo.classrooms(classroomsId),
gradeName INT,
gradeLetter INT, --1A, 2B . . . 
state BIT
)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GO 
CREATE TABLE gradesSubjectsTeacher(
gradeSubjectsTeacherId INT PRIMARY KEY,
gradeId INT FOREIGN KEY REFERENCES dbo.grades(gradeId),
subjectId INT FOREIGN KEY REFERENCES dbo.subject(subjectId),
employeeId INT FOREIGN KEY REFERENCES dbo.employees(employeeId),
schoolYear INT
)

GO
CREATE TABLE horary(
horaryId INT PRIMARY KEY IDENTITY,
gradeSubjectsTeacher INT FOREIGN KEY REFERENCES dbo.gradesSubjectsTeacher(gradeSubjectsTeacherId),
dayWeek INT,
startTime TIME,
finalTime TIME 
)

GO 
CREATE TABLE homework(
homeworkId INT PRIMARY KEY IDENTITY,
gradeSubjectsTeacher INT FOREIGN KEY REFERENCES dbo.gradesSubjectsTeacher(gradeSubjectsTeacherId),
description VARCHAR(100),
forDate DATE,
state INT -- 1 enviada, 2 vencida
)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
GO
CREATE TABLE qualification(
qualificationId INT PRIMARY KEY IDENTITY,
studentId INT FOREIGN KEY REFERENCES dbo.student(studentId),
gradeSubjectsTeacher INT FOREIGN KEY REFERENCES dbo.gradesSubjectsTeacher(gradeSubjectsTeacherId),
acummulated FLOAT,
exam FLOAT,
numberPartial INT,
schoolYear INT
)


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GO
CREATE TABLE duty(
dutyId INT PRIMARY KEY IDENTITY,
dutyName VARCHAR(25),
price MONEY,
state bit
)

GO
CREATE TABLE payments(
paymentId INT PRIMARY KEY IDENTITY,
userId INT FOREIGN KEY REFERENCES dbo.users(userId),
datePayment DATE,
paymentType INT,
totalPayment MONEY
)

GO
CREATE TABLE paymentsDetail(
paymentsDetailId INT PRIMARY KEY IDENTITY,
paymentsId INT FOREIGN KEY REFERENCES dbo.payments(paymentId),
dutyId INT FOREIGN KEY REFERENCES dbo.duty(dutyId),
cost MONEY,
typePayments BIT
)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GO
CREATE TABLE enrollment(
enrollmentId INT PRIMARY KEY IDENTITY,
paymentsDetailsId INT FOREIGN KEY REFERENCES dbo.paymentsDetail(paymentsDetailId),
studentId INT FOREIGN KEY REFERENCES dbo.student(studentId),
gradeId INT FOREIGN KEY REFERENCES dbo.grades(gradeId),
dateEnrollment DATE,
schoolYear INT
)

GO
CREATE TABLE monthlyStudents(
monthlyStudentsId INT PRIMARY KEY IDENTITY,
enrollmentId INT FOREIGN KEY REFERENCES dbo.enrollment(enrollmentId),
month VARCHAR(20),
surchage MONEY,
monthlyPayment MONEY,
state BIT 
)
